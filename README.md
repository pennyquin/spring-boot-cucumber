# Prueba de Base de Datos Cucumber + Serenity + Screenplay Pattern + Spring Boot
Este proyecto realiza pruebas de consultas a una base de datos con una tabla
con informacion de libros. Cubre el ejercicio numero 3 asignado.

La base de datos es una base de datos h2, que esta embebida  dentro de Spring Boot

## Compilación de preubas

Este es un proyecto Maven. Para ejecutar pruebas

`mvn clean verify`
