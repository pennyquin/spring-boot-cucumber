package com.pennyquin.springbootcucumber.questions;

import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.repository.BookRepository;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class InsertResult implements Question<Integer> {

    private BookRepository bookRepository;

    @Override
    public Integer answeredBy(Actor actor) {
        bookRepository = QueryADatabase.as(actor).getBookRepository();
        return bookRepository.findAll().size();
    }

    public static Question<Integer> value(){
        return new InsertResult();
    }
}
