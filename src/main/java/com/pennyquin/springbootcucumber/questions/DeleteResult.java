package com.pennyquin.springbootcucumber.questions;

import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.repository.BookRepository;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class DeleteResult implements Question<Boolean> {

    private BookRepository bookRepository;
    private Long id;

    @Override
    public Boolean answeredBy(Actor actor) {
        bookRepository = QueryADatabase.as(actor).getBookRepository();
        return bookRepository.findById(id).isPresent();
    }

    public DeleteResult(Long id) {
        this.id = id;
    }

    public static DeleteResult withId(Long id){
        return new DeleteResult(id);
    }


}
