package com.pennyquin.springbootcucumber.questions;

import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.actions.Select;
import com.pennyquin.springbootcucumber.model.Book;
import com.pennyquin.springbootcucumber.repository.BookRepository;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.List;

public class SelectResult implements Question<List<Book>> {

    private BookRepository bookRepository;

    @Override
    public List<Book> answeredBy(Actor actor) {
        bookRepository = QueryADatabase.as(actor).getBookRepository();
        return bookRepository.findAll();
    }

    public static SelectResult allBooks() { return new SelectResult();}
}
