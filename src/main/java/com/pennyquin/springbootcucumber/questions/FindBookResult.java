package com.pennyquin.springbootcucumber.questions;

import com.pennyquin.springbootcucumber.model.Book;
import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.repository.BookRepository;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class FindBookResult implements Question<Book> {

    private BookRepository bookRepository;

    private Long id;

    public FindBookResult(Long id) {
        this.id = id;
    }

    public static FindBookResult withId(Long id){
        return new FindBookResult(id);
    }

    @Override
    public Book answeredBy(Actor actor) {
        bookRepository = QueryADatabase.as(actor).getBookRepository();
        return bookRepository.findById(id).get();
    }
}
