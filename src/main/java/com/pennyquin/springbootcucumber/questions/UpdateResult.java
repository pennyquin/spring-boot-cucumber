package com.pennyquin.springbootcucumber.questions;

import com.pennyquin.springbootcucumber.model.Book;
import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.repository.BookRepository;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class UpdateResult implements Question<Book> {

    private BookRepository bookRepository;

    private Long id;

    public UpdateResult(Long id) {
        this.id = id;
    }

    public static UpdateResult withId(Long id){
        return new UpdateResult(id);
    }

    @Override
    public Book answeredBy(Actor actor) {
        bookRepository = QueryADatabase.as(actor).getBookRepository();
        return bookRepository.findById(id).get();
    }
}
