package com.pennyquin.springbootcucumber.repository;

import com.pennyquin.springbootcucumber.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
