package com.pennyquin.springbootcucumber.actions;

import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.model.Book;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class Insert implements Interaction {


    private Book book;

    public Insert(Book book) {
        this.book = book;
    }

    public <T extends Actor> void performAs(T actor) {
        QueryADatabase.as(actor).getBookRepository().save(book);
    }

    public static Insert theBook(Book book) { return new Insert(book);}
}
