package com.pennyquin.springbootcucumber.actions;

import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;


public class Select implements Interaction {

    private Long bookId;

    public Select(Long bookId) {
        this.bookId = bookId;
    }

    public <T extends Actor> void performAs(T actor) {
        QueryADatabase.as(actor).getBookRepository().getOne(bookId);
    }

    public static Select theBookById(Long bookId) { return new Select(bookId);}
}
