package com.pennyquin.springbootcucumber.actions;

import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.model.Book;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class Delete implements Interaction {

    private Book book;
    private Long id;

    public Delete(Book book) {
        this.book = book;
    }
    public Delete(Long id) {
        this.id = id;
    }

    public <T extends Actor> void performAs(T actor) {
        if (book != null) {
            QueryADatabase.as(actor).getBookRepository().delete(book);
        }
        if (id != null) {
            QueryADatabase.as(actor).getBookRepository().deleteById(id);
        }
    }

    public static Delete theBook(Book book) { return new Delete(book);}

    public static Delete theBookWithId(Long id) { return new Delete(id);}

}
