package com.pennyquin.springbootcucumber.actions;

import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.model.Book;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class DeleteAll implements Interaction {

    private Book book;
    private Long id;


    public <T extends Actor> void performAs(T actor) {
            QueryADatabase.as(actor).getBookRepository().deleteAll();

    }

    public static DeleteAll theBooks() { return new DeleteAll();}

}
