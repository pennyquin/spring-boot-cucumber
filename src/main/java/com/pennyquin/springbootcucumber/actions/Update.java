package com.pennyquin.springbootcucumber.actions;

import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.model.Book;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class Update implements Interaction {

    private Book book;

    public Update(Book book) {
        this.book = book;
    }

    public <T extends Actor> void performAs(T actor) {
        QueryADatabase.as(actor).getBookRepository().save(book);
    }

    public static Update theBook(Book book) { return new Update(book);}
}
