package com.pennyquin.springbootcucumber.abilities;

import com.pennyquin.springbootcucumber.repository.BookRepository;
import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;

public class QueryADatabase implements Ability {

    private BookRepository bookRepository;

    public QueryADatabase(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public static QueryADatabase withRepository(BookRepository bookRepository){
        return new QueryADatabase(bookRepository);
    }

    public static QueryADatabase as(Actor actor)  {
        return actor.abilityTo(QueryADatabase.class);
    }

    public BookRepository getBookRepository() {
        return bookRepository;
    }

}
