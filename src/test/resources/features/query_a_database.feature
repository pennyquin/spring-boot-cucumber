Feature: El usuario quiere hacer consultas en la base de datos
Scenario Outline: Usuario intenta crear un libro en la base de datos
Given Pepito accede a la aplicacion
When él envíe la información para crear un libro con id <id>, nombre <nombre>, autor <autor> y numero de paginas <paginas>
Then él debería ver en base de datos el nuevo libro creado
Examples:
|id |nombre |autor| paginas |
|1|Happy Potter y el Prisionero de Azkaban| J.K Rowling | 360 |
|2|Happy Potter y la orden del fenix| J.K Rowling | 650 |
|3|Happy Potter y las reliquias de la muerta| J.K Rowling | 890 |


Scenario Outline: Usuario intenta actualizar un libro en la base de datos
Given Pepito accede a la aplicacion
When él envíe la información para actualizar un libro con id <id>, nombre <nombre>, autor <autor> y numero de paginas <paginas>
Then él debería ver en base de datos el libro con id <id> actualizado
Examples:
|id |nombre |autor| paginas |
|1|Happy Potter y la piedra filosofal| J.K Rowling | 360 |

Scenario Outline: Usuario intenta visualizar un libro en la base de datos
Given Pepito accede a la aplicacion
When él envíe la información para visualizar un libro por id <id>
Then él debería ver la informacion en base de datos el libro con id <id>
Examples:
|id |
|1|

Scenario Outline: Usuario intenta eliminar un libro en la base de datos
Given Pepito accede a la aplicacion
When él envíe la información para eliminar un libro con id <id>
Then él debería ver en base de datos que el libro con id <id> no existe
Examples:
|id |
|1|

Scenario: Usuario intenta eliminar todos los libros en la base de datos
Given Pepito accede a la aplicacion
When él envíe la información para eliminar todos los libros
Then él debería ver en base de datos que no hay libros
