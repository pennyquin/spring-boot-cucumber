package com.pennyquin.springbootcucumber;

import com.pennyquin.springbootcucumber.actions.*;
import com.pennyquin.springbootcucumber.model.Book;
import com.pennyquin.springbootcucumber.questions.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.pennyquin.springbootcucumber.abilities.QueryADatabase;
import com.pennyquin.springbootcucumber.repository.BookRepository;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.*;


public class QueryDatabaseSteps extends SpringBootBaseIntegrationTest {

    @Autowired
    private BookRepository bookRepository;

    private Book theBookInSpotlight;
    private Book theBookBeforeUpdating;

    @Before
    public void setTheStageScreenplay(){
        OnStage.setTheStage(new OnlineCast());
    }


    @Given("^(.*) accede a la aplicacion$")
    public void call_actor(String actor){
        theActorCalled(actor);
        theActorInTheSpotlight()
                .whoCan(QueryADatabase.withRepository(bookRepository));

    }

    @When("^él envíe la información para crear un libro con id (.*), nombre (.*), autor (.*) y numero de paginas (.*)$")
    public void create_book(Long id,String nombre, String autor, String paginas){
        theActorInTheSpotlight()
                .attemptsTo(Insert.theBook(new Book(id,nombre,autor,Integer.valueOf(paginas))));
        theActorInTheSpotlight().remember("currentId", id);
    }

    @Then("^él debería ver en base de datos el nuevo libro creado$")
    public void user_should_be_able_to_see_that_book_is_stored(){
        Long id = theActorInTheSpotlight().recall("currentId");
        theActorInTheSpotlight()
                .should(seeThat(InsertResult.value(), is(id.intValue())));

    }

    @When("^él envíe la información para actualizar un libro con id (.*), nombre (.*), autor (.*) y numero de paginas (.*)$")
    public void update_book(Long id,String nombre, String autor, Integer paginas){
        theBookInSpotlight = new Book(id,nombre,autor,paginas);
        theBookBeforeUpdating = FindBookResult.withId(id).answeredBy(theActorInTheSpotlight());
        theActorInTheSpotlight()
                .attemptsTo(Update.theBook(theBookInSpotlight));
    }

    @Then("^él debería ver en base de datos el libro con id (.*) actualizado")
    public void user_should_be_able_to_see_that_book_is_update(Long id){
        theActorInTheSpotlight()
                .should(seeThat(UpdateResult.withId(id), samePropertyValuesAs(theBookInSpotlight)));
        theActorInTheSpotlight()
                .should(seeThat(UpdateResult.withId(id), book -> !book.getName().equals(theBookBeforeUpdating.getName())));

    }

    @When("^él envíe la información para visualizar un libro por id (.*)$")
    public void see_book(Long id){
        theActorInTheSpotlight()
                .attemptsTo(Select.theBookById(id));
    }

    @Then("^él debería ver la informacion en base de datos el libro con id (.*)$")
    public void user_should_be_able_to_see_book_information(Long id){
        theActorInTheSpotlight()
                .should(seeThat(FindBookResult.withId(id), notNullValue(Book.class)));

    }

    @When("^él envíe la información para eliminar un libro con id (.*)$")
    public void delete_book(Long id){
        theActorInTheSpotlight()
                .attemptsTo(Delete.theBookWithId(id));
    }

    @Then("^él debería ver en base de datos que el libro con id (.*) no existe")
    public void user_should_be_able_to_see_that_book_is_deleted(Long id){
        theActorInTheSpotlight()
                .should(seeThat(DeleteResult.withId(id), is(false)));


    }

    @When("^él envíe la información para eliminar todos los libros$")
    public void delete_all_books(){
        theActorInTheSpotlight()
                .attemptsTo(DeleteAll.theBooks());
    }

    @Then("^él debería ver en base de datos que no hay libros")
    public void user_should_be_able_to_see_that_there_is_no_books(){
        theActorInTheSpotlight()
                .should(seeThat(SelectResult.allBooks(), empty()));


    }
}
