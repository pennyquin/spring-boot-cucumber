package com.pennyquin.springbootcucumber;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/query_a_database.feature", plugin = {"pretty", "html:target/cucumber"})
public class StepsSimpleRunner {
}
